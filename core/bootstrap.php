<?php



$config = require 'database/config.php';

require 'database/Connection.php';
require 'database/QueryBuilder.php';
require 'Router.php';

$db =  new QueryBuilder(
	Connection::make($config['database'])
);