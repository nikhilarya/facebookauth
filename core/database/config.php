<?php

return [
	'database' => [
		'name' => 'social_media',
		'username' => 'root',
		'password' => '',
		'connection' => 'mysql:host=localhost',
		'option' => [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		]
	]
];