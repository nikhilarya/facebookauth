window.fbAsyncInit = function() {
    FB.init({
      appId            : '588184498193457',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.12'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));


// // on click 
function login() {
  FB.login(function (response) {
      console.log(response);
    })
  }

  

//---------------------------------------------------------------------------------------------------------------------------------------

// window.fbAsyncInit = function () {
//   FB.init({
//     appId: '588184498193457',
//     status: true,
//     cookie: true,
//     xfbml: true
//   });
// };

// (function (doc) {
//   var js;
//   var id = 'facebook-jssdk';
//   var ref = doc.getElementsByTagName('script')[0];
//   if (doc.getElementById(id)) {
//     return;
//   }
//   js = doc.createElement('script');
//   js.id = id;
//   js.async = true;
//   js.src = "//connect.facebook.net/en_US/all.js";
//   ref.parentNode.insertBefore(js, ref);
// }(document));

// function login() {
//   FB.login(function (response) {
//     if (response.authResponse) {
//       // some code here
      
//     } else {
//       alert("Login attempt failed!");
//     }
//   }, { scope: 'email,user_photos,publish_actions' });

// }

/**
 login with Google Account
**/
// After the platform library loads, load the auth2 library
// function init() {
//   gapi.load('auth2', function() { // Ready. });
// }

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}

// signOut Function
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        
        alert("You have been successfully signed out");

        $(".g-signin2").css("display", "block");

    });
}
