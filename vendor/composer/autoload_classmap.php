<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'core\\form\\Login' => $baseDir . '/core/form/Login.php',
    'core\\form\\Register' => $baseDir . '/core/form/Register.php',
);
