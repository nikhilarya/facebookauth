<?php //ini_set('display_errors','1'); ?>
<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta name="google-signin-client_id" content="238449898298-chhite8avcu5fdb39374a3e2uhdibv9s.apps.googleusercontent.com">
    <!-- <script src="https://apis.google.com/js/platform.js" async defer></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>
</head>
<body>
    <h1>Register Form</h1>
    <form action="./../../controllers/form/register_controller.php" method="post">
            <label>UserName:</label>
            <input type="text" name="username" placeholder="Enter Username" required=""><br />
            <label>Email:</label>
            <input type="email" name="email" placeholder="Enter your email" required><br />
            <label>Password:</label>
            <input type="password" name="pass" placeholder="Your pasword" required><br />
            <label>Confirm Password:</label>
            <input type="password" name="c_pass" placeholder="Confirm Password" required><br />
            Already a Member? <a href="login.view.php">Go Login</a>
            <br />
            <input type="submit" name="submit" value="Register">
            <br>
            <input type="button" onclick="login()" name="fsubmit" value="Login with facebook">
            <div class="g-signin2" data-onsuccess="onSignIn"></div>
            
    </form>
    <script type="text/javascript" src="./../../public/js/main.js"></script>
</body>
</html>